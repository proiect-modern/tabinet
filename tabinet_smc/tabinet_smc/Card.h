#pragma once
#include <string>
#include <iostream>
#include <io.h>
#include <fcntl.h>
#include <string>
constexpr auto SPADE = L"\u2660";
constexpr auto  CLUB = L"\u2663";
constexpr auto  HEART = L"\u2665";
constexpr auto  DIAMOND = L"\u2666";
constexpr auto  UPPER = L"\x203E";

class Card
{
private:
	int value;
	std::string cardSimbol;
	bool activated;
public:
	Card(int newValue, std::string newSimbol);
	Card();
	~Card();
	Card& operator =(const Card& newCard)
	{
		this->cardSimbol = newCard.cardSimbol;
		this->value = newCard.value;
		return *this;
	}

	int GetValue() const;
	void SetValue(int newValue);
	std::string GetSimbol() const;
	void SetSimbol(std::string newSimbol);
	void DisplayCard();
	bool IsActivated();
	void Activate();
};

