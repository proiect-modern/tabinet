#include "Player.h"

Player::Player(std::vector<Card> newCards)
	:cards(newCards) {}

Player::Player()
	: score(0), cards(std::vector<Card>(0)) {}

Player::~Player() {}

std::vector<Card> Player::GetCards()
{
	return this->cards;
}

void Player::SetCards(std::vector<Card> newCards)
{
	this->cards = newCards;
}

void Player::AddCard(Card newCard)
{
	this->cards.push_back(newCard);
}

void Player::CaptureCard(Card newCard)
{
	this->capturedCards.push_back(newCard);
}

void Player::RemoveCard(int index)
{
	if (index < this->cards.size() && index >= 0)
	{
		this->cards.erase(this->cards.begin() + index);
	}
	else
	{
		std::wcout << "Nu s-a putut scoate cartea din pachetul jucatorului, reincearca! \n";
	}
}

void Player::DisplayAllCards()
{
	std::wcout << "\n";
	for (int index = 0;index < this->cards.size();index++)
	{
		std::wcout << " C" << index + 1 << " ";
		if (this->cards[index].GetValue() == 10)
			std::wcout << " ";
	}
	std::wcout << "\n";
	for (int index = 0;index < this->cards.size();index++)
	{
		std::wcout << "._";
		if (this->cards[index].GetValue() == 10)
			std::wcout << "_";
		std::wcout << ". ";
	}
	std::wcout << "\n";
	for (int index = 0;index < this->cards.size();index++)
	{
		std::wcout << "|";
		if (this->cards[index].GetValue() <= 10)
			std::wcout << this->cards[index].GetValue();
		else
		{
			switch (this->cards[index].GetValue())
			{
			case 11:
				std::wcout << "A";
				break;
			case 12:
				std::wcout << "J";
				break;
			case 13:
				std::wcout << "Q";
				break;
			case 14:
				std::wcout << "K";
				break;
			}
		}
		std::wcout << "| ";
	}
	std::wcout << "\n";
	_setmode(_fileno(stdout), _O_U16TEXT);

	for (int index = 0;index < this->cards.size();index++)
	{
		std::wcout << "|";
		if (this->cards[index].GetSimbol() == "inimaNeagra")
		{
			std::wcout << SPADE;
		}
		if (this->cards[index].GetSimbol() == "inimaRosie")
		{
			std::wcout << HEART;
		}
		if (this->cards[index].GetSimbol() == "trefla")
		{
			std::wcout << CLUB;
		}
		if (this->cards[index].GetSimbol() == "romb")
		{
			std::wcout << DIAMOND;
		}
		if (this->cards[index].GetValue() == 10)
			std::wcout << " ";
		std::wcout << "| ";
	}
	std::wcout << "\n";
	for (int index = 0;index < this->cards.size();index++)
	{
		std::wcout << "'" << UPPER;
		if (this->cards[index].GetValue() == 10)
			std::wcout << UPPER;
		std::wcout << "' ";
	}
	std::wcout << "\n";
}

void Player::CalculateScore()
{
	for (int index = 0;index < this->capturedCards.size();index++)
	{
		if (this->capturedCards[index].GetValue() >= 10 && this->capturedCards[index].GetValue() <= 14)
		{
			this->score++;
		}

		if (this->capturedCards[index].GetValue() == 2 && this->capturedCards[index].GetSimbol() == "trefla")
		{
			this->score += 2;
		}
		if (this->capturedCards[index].GetValue() == 10 && this->capturedCards[index].GetSimbol() == "romb")
		{
			this->score++;
		}
		if (this->capturedCards[index].GetSimbol() == "trefla")
		{
			this->score++;
		}
	}
}

void Player::AddScore(int index)
{
	this->score += index;
}

std::vector<Card> Player::GetCapturedCards() const
{
	return this->capturedCards;
}

int Player::GetScore() const
{
	return this->score;
}
