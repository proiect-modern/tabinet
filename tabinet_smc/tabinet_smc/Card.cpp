#include "Card.h"

Card::Card(int newValue, std::string newSimbol)
	:cardSimbol(newSimbol), value(newValue), activated(false) {}

Card::Card() {}

Card::~Card() {}

int Card::GetValue() const
{
	return this->value;
}

void Card::SetValue(int newValue)
{
	this->value = newValue;
}

std::string Card::GetSimbol() const
{
	return this->cardSimbol;
}

void Card::SetSimbol(std::string newSimbol)
{
	this->cardSimbol = newSimbol;
}

void Card::DisplayCard()
{
	std::wcout << "._." << std::endl << "|";
	if (this->value <= 10)
		std::wcout << this->value;
	else
	{
		switch (this->value)
		{
		case 11:
			std::wcout << "A";
			break;
		case 12:
			std::wcout << "J";
			break;
		case 13:
			std::wcout << "Q";
			break;
		case 14:
			std::wcout << "K";
			break;
		}
	}
	std::wcout << "|" << std::endl << "|";
	_setmode(_fileno(stdout), _O_U16TEXT);
	if (this->cardSimbol == "inimaNeagra")
	{
		std::wcout << SPADE;
	}
	if (this->cardSimbol == "inimaRosie")
	{
		std::wcout << HEART;
	}
	if (this->cardSimbol == "trefla")
	{
		std::wcout << CLUB;
	}
	if (this->cardSimbol == "romb")
	{
		std::wcout << DIAMOND;
	}
	std::wcout << "|" << std::endl << "'" << UPPER << "'";
}

bool Card::IsActivated()
{
	return this->activated;
}

void Card::Activate()
{
	this->activated = true;
}
