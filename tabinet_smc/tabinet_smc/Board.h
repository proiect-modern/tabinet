#pragma once
#include "Player.h"
#include <cstdlib>
#include <ctime>
class Board
{
private:
	std::vector<Player> players;
	std::vector<Card> tableCards;
	std::vector<Card> cardPack;
	int nrOfPlayers;
	int lastPicker;

public:

	Board();
	~Board();
	Board& operator =(Board& temporaryBoard)
	{
		this->cardPack = temporaryBoard.cardPack;
		this->players = temporaryBoard.players;
		this->tableCards = temporaryBoard.tableCards;
		return *this;
	}
	void play();

private:

	void generateCardPack();
	void setNumberOfPlayers();
	int generateRandomNumber(int size);
	void generateFirstPlayerCards();
	void generateAllPlayers();
	void completeFirstPlayerCard();
	void generateBoardCards();
	void displayBoardCards();

	void displayTurnMessage(int index);
	void dropCard(int index);
	void pickCards(int playerIndex, std::vector< std::vector<int>> allMoves);
	void generatePossibleCombinations(int offset, int k, std::vector<int> indexes, std::vector<int> combination, std::vector<std::vector<int>>& possibleMovements);
	bool validateOption(int index, std::vector<int> move);
	void deleteInvalidMovements(int playerIndex, std::vector< std::vector<int>>& allMoves);
	int calculateOptionPoints(std::vector<int> cards);
	void ordonareOptiuni(std::vector<std::vector<int>>& possibleMovements);
	void refillHands(bool& isPlaying);
	void displayPlayersScoreAndWinner();
};

