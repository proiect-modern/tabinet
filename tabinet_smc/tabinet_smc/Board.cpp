#include "Board.h"
#include <random>

Board::Board()
	: players(std::vector<Player>(0)), tableCards(std::vector<Card>(0)), cardPack(std::vector<Card>(0))
{
	this->setNumberOfPlayers();
	this->generateCardPack();
	this->generateFirstPlayerCards();
	this->generateAllPlayers();
	this->completeFirstPlayerCard();
	this->generateBoardCards();
}

Board::~Board()
{
}

void Board::play()
{
	bool isPlaying = true;
	int playerIndex = 0;
	std::vector<std::vector<int>> possibleMovements;
	std::vector<int> indexes;
	std::vector<int> combination;

	while (isPlaying)
	{
		if (playerIndex == this->players.size())
		{
			playerIndex = 0;
		}
		indexes.clear();
		possibleMovements.clear();
		combination.clear();

		if (this->players[playerIndex].GetCards().size() > 0)
		{
			for (int index = 0;index < this->tableCards.size();index++)
			{
				indexes.push_back(index);
			}

			for (int index = 1;index <= indexes.size();index++)
			{
				this->generatePossibleCombinations(0, index, indexes, combination, possibleMovements);
			}
			this->deleteInvalidMovements(playerIndex, possibleMovements);
			if (possibleMovements.size() > 1)
			{
				this->ordonareOptiuni(possibleMovements);
			}
			this->displayTurnMessage(playerIndex);
			bool choice = true;
			if (possibleMovements.size() == 0)
			{
				choice = false;
				std::wcout << "\nNu exista perechi de carti ce pot fi ridicate\n";
			}
			if (choice == false)
			{
				this->dropCard(playerIndex);
			}
			else
			{
				this->pickCards(playerIndex, possibleMovements);
				if (this->tableCards.size() == 0)
				{
					this->players[playerIndex].AddScore(1);
				}
				this->lastPicker = playerIndex;
			}
		}
		else
		{
			std::wcout << "\n Playerul " << playerIndex << " are mana goala, se continua jocul pana la reimpartirea cartilor ! \n\n";
		}
		playerIndex++;
		refillHands(isPlaying);
	}
	for (auto& tableCard : this->tableCards)
	{
		this->players[this->lastPicker].CaptureCard(tableCard);
	}
	this->tableCards.clear();
	this->displayPlayersScoreAndWinner();
}

void Board::generateCardPack()
{
	for (int index = 2;index <= 14;index++)
	{
		Card c1(index, "inimaNeagra");
		Card c2(index, "inimaRosie");
		Card c3(index, "romb");
		Card c4(index, "trefla");
		this->cardPack.push_back(c1);
		this->cardPack.push_back(c2);
		this->cardPack.push_back(c3);
		this->cardPack.push_back(c4);
	}
}

void Board::setNumberOfPlayers()
{
	std::wcout << "\n" << "In cati playeri doriti sa jucati? (2/3/4) \n";
	std::wcout << "Numar de playeri:";
	int x;
	std::cin >> x;
	if (x >= 2 && x <= 4)
	{
		this->nrOfPlayers = x;
		return;
	}
	else
	{
		std::wcout << "Valoare INVALIDA, rescrieti!\n";
		setNumberOfPlayers();
	}
}

int Board::generateRandomNumber(int size)
{
	size--;
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::uniform_int_distribution<> distribution(0, size);
	return distribution(generator);
}

void Board::generateFirstPlayerCards()
{
	Player x;
	for (int index = 0;index < 4;index++)
	{
		int p = generateRandomNumber(this->cardPack.size());
		x.AddCard(this->cardPack[p]);
		this->cardPack.erase(this->cardPack.begin() + p);
	}
	std::wcout << "--------------------------------------------------------------------------------------------------\n \n";
	std::wcout << "Primul player are urmatoarele carti : \n";
	x.DisplayAllCards();
	std::wcout << "Pastrezi cartile?(da/nu)\nRaspuns:";
	std::string raspuns;
	std::cin >> raspuns;
	if (raspuns == "da")
	{
		this->players.push_back(x);
	}
	else
	{
		Player y;
		for (int index = 0;index < 6;index++)
		{
			int p = generateRandomNumber(this->cardPack.size());
			y.AddCard(this->cardPack[p]);
			this->cardPack.erase(this->cardPack.begin() + p);
		}
		this->players.push_back(y);
	}
}

void Board::generateAllPlayers()
{
	for (int index = 1;index < this->nrOfPlayers;index++)
	{
		Player y;
		for (int index = 0;index < 6;index++)
		{
			int p = generateRandomNumber(this->cardPack.size());
			y.AddCard(this->cardPack[p]);
			this->cardPack.erase(this->cardPack.begin() + p);
		}
		this->players.push_back(y);
	}
}

void Board::completeFirstPlayerCard()
{
	if (players[0].GetCards().size() < 6)
	{
		int p = generateRandomNumber(this->cardPack.size());
		players[0].AddCard(this->cardPack[p]);
		this->cardPack.erase(this->cardPack.begin() + p);
		p = generateRandomNumber(this->cardPack.size());
		players[0].AddCard(this->cardPack[p]);
		this->cardPack.erase(this->cardPack.begin() + p);
	}
}

void Board::generateBoardCards()
{
	for (int index = 0;index < 4;index++)
	{
		int p = generateRandomNumber(this->cardPack.size());
		this->tableCards.push_back(this->cardPack[p]);
		this->cardPack.erase(this->cardPack.begin() + p);
	}
}

void Board::displayBoardCards()
{
	std::wcout << "\nBOARD\n.";
	std::wcout << "\n| ";
	for (int index = 0;index < this->tableCards.size();index++)
	{
		std::wcout << " C" << index + 1 << " ";
		if (this->tableCards[index].GetValue() == 10)
			std::wcout << " ";
	}
	std::wcout << " |\n| ";
	for (int index = 0;index < this->tableCards.size();index++)
	{
		std::wcout << "._";
		if (this->tableCards[index].GetValue() == 10)
			std::wcout << "_";
		std::wcout << ". ";
	}
	std::wcout << " |\n| ";
	for (auto& tableCard : this->tableCards)
	{
		std::wcout << "|";
		if (tableCard.GetValue() <= 10)
			std::wcout << tableCard.GetValue();
		else
		{
			switch (tableCard.GetValue())
			{
			case 11:
				std::wcout << "A";
				break;
			case 12:
				std::wcout << "J";
				break;
			case 13:
				std::wcout << "Q";
				break;
			case 14:
				std::wcout << "K";
				break;
			}
		}
		std::wcout << "| ";
	}
	std::wcout << " |\n| ";
	_setmode(_fileno(stdout), _O_U16TEXT);

	for (auto& tableCard : this->tableCards)
	{
		std::wcout << "|";
		if (tableCard.GetSimbol() == "inimaNeagra")
		{
			std::wcout << SPADE;
		}
		if (tableCard.GetSimbol() == "inimaRosie")
		{
			std::wcout << HEART;
		}
		if (tableCard.GetSimbol() == "trefla")
		{
			std::wcout << CLUB;
		}
		if (tableCard.GetSimbol() == "romb")
		{
			std::wcout << DIAMOND;
		}
		if (tableCard.GetValue() == 10)
			std::wcout << " ";
		std::wcout << "| ";
	}
	std::wcout << " |\n| ";
	for (auto& tableCard : this->tableCards)
	{
		std::wcout << "'" << UPPER;
		if (tableCard.GetValue() == 10)
			std::wcout << UPPER;
		std::wcout << "' ";
	}
	std::wcout << " |\n.\n";
}

void Board::displayTurnMessage(int index)
{
	std::wcout << "\n--------------------------------------------------------------------------------------------------\n";
	std::wcout << "\nIn pachet mai sunt " << this->cardPack.size() << " carti \n\n";
	std::wcout << "\nEste randul jucatorului " << index + 1 << " cu urmatoarele carti\n\n";
	this->displayBoardCards();
	std::wcout << "\nMANA\n";
	this->players[index].DisplayAllCards();
	std::wcout << "\n";
}

void Board::dropCard(int index)
{
	std::wcout << "\n\nCe carte doresti sa lasi? (pozitia cartii) :";
	int x;
	std::cin >> x;
	if (x <= this->players[index].GetCards().size() && x > 0)
	{
		this->tableCards.push_back(this->players[index].GetCards()[x - 1]);
		this->players[index].RemoveCard(x - 1);
	}
	else
	{
		std::wcout << "INPUT INVALID,RESCRIE!\n";
		dropCard(index);
		return;
	}
	this->displayBoardCards();
}

void Board::pickCards(int playerIndex, std::vector< std::vector<int>> allMoves)
{
	std::wcout << "Optiunile de ridicare a unor carti de pe masa sunt urmatoarele, ordonate dupa valoarea in puncte:\n";
	int index = 1;
	for (auto& move : allMoves)
	{
		std::wcout << index << " -> ";
		for (int jdex = 0; jdex < move.size();jdex++)
		{
			std::wcout << "C" << move[jdex] + 1 << " ";
		}
		std::wcout << "\n";
		index++;
	}

	int option;
	std::wcout << "Ce optiune alegi? (scrie index-ul ei )\nOptiune:";
	std::cin >> option;
	if (option > allMoves.size() || option < 1)
	{
		std::wcout << "IMDEX INVALID \n\n";
		pickCards(playerIndex, allMoves);
		return;
	}
	option--;
	int sum = 0;
	int contor = 0;
	for (auto& move : allMoves[option])
	{
		sum += this->tableCards[move - contor].GetValue();
		this->players[playerIndex].CaptureCard(this->tableCards[move - contor]);
		this->tableCards.erase(this->tableCards.begin() + (move - contor));
		contor++;
	}

	contor = 0;
	std::vector<Card> hand = this->players[playerIndex].GetCards();
	for (int index = 0;index < hand.size();index++)
	{
		if (hand[index].GetValue() == sum)
		{
			this->players[playerIndex].CaptureCard(hand[index]);
			this->players[playerIndex].RemoveCard(index - contor);
			contor++;
		}
	}
}


void Board::generatePossibleCombinations(int offset, int k, std::vector<int> indexes, std::vector<int> combination, std::vector<std::vector<int>>& possibleMovements) {
	if (k == 0) {
		possibleMovements.push_back(combination);
		return;
	}
	for (int i = offset; i <= indexes.size() - k; ++i) {
		combination.push_back(indexes[i]);
		generatePossibleCombinations(i + 1, k - 1, indexes, combination, possibleMovements);
		combination.pop_back();
	}
}


bool Board::validateOption(int index, std::vector<int> move)
{
	int sum = 0;
	int nrOfAs = 0;
	for (auto& eachMove : move)
	{
		if (this->tableCards[eachMove].GetValue() != 11)
		{
			sum += this->tableCards[eachMove].GetValue();
		}
		else
		{
			nrOfAs++;
		}

	}
	std::vector<Card> hand = this->players[index].GetCards();
	sum += nrOfAs;
	for (auto& handCard : hand)
	{
		if (handCard.GetValue() == sum)
		{
			return true;
		}
	}

	if (nrOfAs >= 1)
	{
		sum += 10;
		for (auto& handCard : hand)
		{
			if (handCard.GetValue() == sum)
			{
				return true;
			}
		}
	}
	return false;
}


void Board::deleteInvalidMovements(int playerIndex, std::vector<std::vector<int>>& allMoves)
{
	for (int index = 0;index < allMoves.size();index++)
	{
		std::vector<int > move = allMoves[index];
		if (this->validateOption(playerIndex, move) == false)
		{
			allMoves.erase(allMoves.begin() + index);
			index--;
		}
	}
}

int Board::calculateOptionPoints(std::vector<int> cards)
{
	int sum = 0;
	for (auto& card : cards)
	{
		if (this->tableCards[card].GetValue() == 14 || this->tableCards[card].GetValue() == 13 || this->tableCards[card].GetValue() == 12 || this->tableCards[card].GetValue() == 11 || this->tableCards[card].GetValue() == 10)
		{
			sum++;
		}
		if (this->tableCards[card].GetValue() == 10 && this->tableCards[card].GetSimbol() == "romb")
		{
			sum++;
		}
		if (this->tableCards[card].GetValue() == 2 && this->tableCards[card].GetSimbol() == "trefla")
		{
			sum += 2;
		}
	}
	return sum;
}

void Board::ordonareOptiuni(std::vector<std::vector<int>>& possibleMovements)
{
	for (int index = 0;index < possibleMovements.size() - 1;index++)
	{
		for (int jdex = index + 1;jdex < possibleMovements.size();jdex++)
		{
			if (calculateOptionPoints(possibleMovements[index]) <= calculateOptionPoints(possibleMovements[jdex]))
			{
				std::vector<int> aux = possibleMovements[index];
				possibleMovements[index] = possibleMovements[jdex];
				possibleMovements[jdex] = aux;
				aux.clear();
			}
		}
	}
}

void Board::refillHands(bool& isPlaying)
{
	int playersWithNoCards = 0;
	for (auto& player : this->players)
	{
		if (player.GetCards().size() == 0)
		{
			playersWithNoCards++;
		}
	}
	if (playersWithNoCards == this->players.size())
	{
		if (this->cardPack.size() == 0)
		{
			std::wcout << "\n\n\n---------- JOCUL S-A ICHEIAT ----------\n\n\n";
			isPlaying = false;
		}
		else
		{
			if (this->cardPack.size() >= this->players.size() * 6)
			{
				for (int index = 0;index < this->players.size();index++)
				{
					for (int jdex = 0;jdex < 6;jdex++)
					{
						int p = generateRandomNumber(this->cardPack.size());
						this->players[index].AddCard(this->cardPack[p]);
						this->cardPack.erase(this->cardPack.begin() + p);
					}
				}
			}
			else
			{
				int playerIndex = 0;
				while (this->cardPack.size())
				{
					if (playerIndex == this->players.size())
					{
						playerIndex = 0;
					}

					this->players[playerIndex].AddCard(this->cardPack[this->cardPack.size() - 1]);
					this->cardPack.pop_back();
					playerIndex++;
				}
			}
		}
	}
}

void Board::displayPlayersScoreAndWinner()
{
	int maxNrOfCards = 0;
	int maxPlayer;
	for (int index = 0;index < this->players.size();index++)
	{
		this->players[index].CalculateScore();
		if (this->players[index].GetCapturedCards().size() > maxNrOfCards)
		{
			maxNrOfCards = this->players[index].GetCapturedCards().size();
			maxPlayer = index;
		}
	}
	this->players[maxPlayer].AddScore(3);

	int maxScore = 0;
	int winner;
	for (int index = 0;index < this->players.size();index++)
	{
		std::wcout << "\n Jucatorul  " << index + 1 << " are scorul " << this->players[index].GetScore() << " \n";
		if (this->players[index].GetScore() > maxScore)
		{
			maxScore = this->players[index].GetScore();
			winner = index;
		}
	}
	std::wcout << "\n\n\n--------- CASTIGATORUL ESTE PLAYER " << winner + 1 << "  FELICITARI! ------------------\n\n\n\n";
}

