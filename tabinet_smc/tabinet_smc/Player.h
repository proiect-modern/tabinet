#pragma once
#include "Card.h"
#include <iostream>
#include <vector>
class Player
{
private:
	std::vector<Card> cards;
	std::vector<Card> capturedCards;
	int score;
public:
	Player(std::vector<Card> newCards);
	Player();
	~Player();

	Player& operator =(Player& newPlayer)
	{
		this->cards = newPlayer.cards;
		return *this;
	}

	std::vector<Card> GetCards();
	void SetCards(std::vector<Card> newCards);
	void AddCard(Card newCard);
	void CaptureCard(Card newCard);
	void RemoveCard(int index);
	void DisplayAllCards();
	void CalculateScore();
	void AddScore(int index);
	std::vector<Card> GetCapturedCards() const;
	int GetScore() const;
};

